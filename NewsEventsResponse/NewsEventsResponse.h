//
//  NewsEventsResponse.h
//  Desi
//
//  Created by Jai on 01/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsEventsResponse : NSObject
@property(nonatomic, strong) NSString *link, *title, *type;
@property(nonatomic, strong) NSDate *date;

-(id)initWithObject:(id)obj;
-(void)printAllValues;

@end
