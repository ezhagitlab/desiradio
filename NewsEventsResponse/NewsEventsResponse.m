//
//  NewsEventsResponse.m
//  Desi
//
//  Created by Jai on 01/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "NewsEventsResponse.h"
#import "Logger.h"

@implementation NewsEventsResponse

-(id)initWithObject:(id)obj
{
    self = [super init];
    
    if(self)
    {
        [self parse:obj];
    }
    return self;
}

-(void)parse:(id)responseObject{
    
    self.date   = [responseObject objectForKey:@"date"];
    self.link   = [responseObject objectForKey:@"link"];
//    self.link = @"www.google.com";
    self.title  = [responseObject objectForKey:@"title"];
//    self.title  = @"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia xdeserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.";
    self.type   = [responseObject objectForKey:@"type"];
}

-(void)printAllValues
{
    TRC_DBG(@"Date %@", self.date);
    TRC_DBG(@"Link %@", self.link);
    TRC_DBG(@"Title %@", self.title);
    TRC_DBG(@"Type %@", self.type);

}
@end
