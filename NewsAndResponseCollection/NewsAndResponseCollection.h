//
//  NewsAndResponseCollection.h
//  Desi
//
//  Created by Jai on 01/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsAndResponseCollection : NSObject

@property(nonatomic, strong)NSMutableArray *newsArray, *eventsArray;

-(id)initWithObject:(id)obj;
-(void)printAllValues;

@end
