//
//  NewsAndResponseCollection.m
//  Desi
//
//  Created by Jai on 01/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "NewsAndResponseCollection.h"
#import "NewsEventsResponse.h"

@implementation NewsAndResponseCollection

-(id)initWithObject:(id)obj
{
    if(self = [super init])
    {
        [self parse:obj];
    }
    return self;
}

-(NSMutableArray*)newsArray
{
    if(!_newsArray)
    {
        _newsArray = [[NSMutableArray alloc]init];
    }
    return _newsArray;
}

-(NSMutableArray*)eventsArray
{
    if(!_eventsArray)
    {
        _eventsArray = [[NSMutableArray alloc]init];
    }
    return _eventsArray;
}


-(void)parse:(id)responseObject
{
    NSArray *array = [NSArray arrayWithArray:responseObject];
    
    for (id obj in array)
    {
        NewsEventsResponse *newsRes = [[NewsEventsResponse alloc]initWithObject:obj];
        
        if([@"news" isEqualToString:newsRes.type])
        {
            [self.newsArray addObject:newsRes];
        }
        else if([@"events" isEqualToString:newsRes.type])
        {
            [self.eventsArray addObject:newsRes];
        }
    }
}

-(void)printAllValues
{
    for (NewsEventsResponse *obj in self.newsArray) {
        [obj printAllValues];
    }
}
@end
