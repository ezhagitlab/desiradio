
//
//  main.m
//  Desi
//
//  Created by Jai on 12/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
