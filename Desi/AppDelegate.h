//
//  AppDelegate.h
//  Desi
//
//  Created by Jai on 12/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) NSString *deviceTokenForPush;

@end
