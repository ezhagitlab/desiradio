//
//  AppDelegate.m
//  Desi
//
//  Created by Jai on 12/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "AppDelegate.h"
#import "Utils.h"
#import "CustomTabBarController.h"
#import "EnumConstants.h"
#import "WebServiceManager.h"
#import "BannerViewController.h"
#import "Constants.h"
#import "Logger.h"

static const NSInteger EXPIRE_DATE = 1;

static const NSString *APS_KEY = @"aps";
static const NSString *OK_BUTTON_TITLE = @"OK";
static const NSString *CANCEL_BUTTON_TITLE = @"Cancel";

static const NSString *kPushTypeNews    = @"news";
static const NSString *kPushTypeEvents  = @"events";

@interface AppDelegate()

@property(nonatomic, strong) CustomTabBarController *tabBarController;


@end

@implementation AppDelegate

-(void)setPurgeDate{

    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"AppPurgeDate"];
}

-(NSDate*)getPurgeDate{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"AppPurgeDate"];
}

-(NSInteger)getDayDifference:(NSDate*)fromDate ToDate:(NSDate*)ToDate{
    return [ToDate timeIntervalSinceDate:fromDate] / (60 * 60 * 24);
}

-(BOOL)purgeApplication{
    
    NSDate *date = [self getPurgeDate];
    if(!date)
    {
        [self setPurgeDate];
    }
    else
    {
        NSInteger days = [self getDayDifference:date ToDate:[NSDate date]];
        if(days >= EXPIRE_DATE)
            return YES;
    }
    return NO;
}

-(void)parseInitialization
{
//    [Parse setApplicationId:@"AqXnbhI8kFuJ2NtAer8l9NgR8qSQAJABR9uU4iVk"
//                  clientKey:@"vILcSxKpvGlBWlhiR4VpjBFHEzhf3PTT9ukKICUI"];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    self.deviceTokenForPush = @"TEST";

    //+++++++++++++++++++++++++++++++ PARSE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /*
    //initialize
    [self parseInitialization];

    //Save parse object
    PFObject *lolcat = [PFObject objectWithClassName:@"Lolcat"];
    [lolcat setObject:@"desi" forKey:@"name"];
    [lolcat save];
    */
    //+++++++++++++++++++++++++++++++++ END +++++++++++++++++++++++++++++++++++++++++++++++++++++++


    //+++++++++++++++++++++++++++++++ Registration for Push notification ++++++++++++++++++++++++++
    
    // Register for push notifications
    [application registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];

    //++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    
    //+++++++++++++++++++++++++++++++ Window initialization +++++++++++++++++++++++++++++++++++++++
    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    //Create CustomTabBar controller
    self.tabBarController = [[CustomTabBarController alloc]init];
    self.tabBarController.delegate = self;
    
    BannerViewController *bVC = [[BannerViewController alloc]initWithContentViewController:self.tabBarController];
    self.window.rootViewController = bVC;

    //+++++++++++++++++++++++++++++++++ END +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    
    
    //+++++++++++++++++++++++++++++++ Push launch options +++++++++++++++++++++++++++++++++++++++++
    
    if (launchOptions){
        
		TRC_DBG(@"launch oprions in didfinish launchibg %@", launchOptions);
        NSDictionary* userInfo = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
		
            if (userInfo)
            {  // if this key is present, we assume push is present
                
                NSInteger actionId = 1;
                TRC_DBG(@"Push notification received: %@", userInfo);

                if([userInfo objectForKey:@"type"])
                    actionId = ([(NSString*)kPushTypeNews isEqualToString:[userInfo objectForKey:@"type"]]) ? 1 : 2;
                [self.tabBarController handlePushForActioId:actionId];
            }
    }
    //+++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)sendDeviceToken
{
    /*
    double rand =  random();
    NSString *deviceToken = [NSString stringWithFormat:@"Test%f", rand];
    */
    
    if(!self.deviceTokenForPush)
    {
        return;
    }
    
    [[WebServiceManager sharedInstance] sendDeviceTokenHandler:self.deviceTokenForPush successBlock:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        TRC_DBG(@"Response %@", [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding]);
    }
    failureBlock:^(AFHTTPRequestOperation *operation, NSError *err)
    {
        TRC_DBG(@"Failure: sending device token %@", err.description);
    }];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    TRC_DBG(@"Registered for push notification DEVICE TOKEN : %@", newDeviceToken);

    self.deviceTokenForPush = [NSString stringWithFormat:@"%@", newDeviceToken];
	self.deviceTokenForPush = [self.deviceTokenForPush stringByTrimmingCharactersInSet:
                                      [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	self.deviceTokenForPush = [self.deviceTokenForPush stringByReplacingOccurrencesOfString:@" " withString:@""];
	TRC_DBG(@"%@", self.deviceTokenForPush);
    
    [self sendDeviceToken];
   
    /*
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
     */
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    TRC_DBG(@"Remote notification received %@", userInfo);
//    [PFPush handlePush:userInfo];
    
//    NSDictionary *dic = [userInfo objectForKey:APS_KEY];
    NSString *title   = @"Message";
    NSString *okTitle = (NSString*)OK_BUTTON_TITLE;
    NSString *cancelTitle = (NSString*)CANCEL_BUTTON_TITLE;
    NSInteger actionId = 1;
    
    if (userInfo)
    {  // if this key is present, we assume push is present
        
        if([userInfo objectForKey:@"type"])
            actionId = ([(NSString*)kPushTypeNews isEqualToString:[userInfo objectForKey:@"type"]]) ? 1 : 2;
        
        if([userInfo objectForKey:@"title"])
            title = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"title"]];
    }
    
    TRC_DBG(@"Title %@", title);
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:title delegate:self cancelButtonTitle:okTitle otherButtonTitles:cancelTitle, nil];

    [alertView setTag:actionId];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        switch (alertView.tag) {
                
            case 1:
                [self.tabBarController handlePushForActioId:alertView.tag];
                break;
                
            case 2:
                [self.tabBarController handlePushForActioId:alertView.tag];
                break;
                
            default:
                break;
        }
    }
}

-(void)makeAudioCall{
    
    NSString *phNo = @"tel://123";
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phNo]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phNo]];
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:@"This device doesn't support" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)makeSkypeAudioCall
{
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]];
    
    if(installed)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"skype:desibeatradio.org?call"]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.com/apps/skype/skype"]];
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
        TRC_DBG(@"Selected index: %d", tabBarController.selectedIndex);
        if(tabBarController.selectedIndex == DesiBeatsItemEvents)
            [self makeSkypeAudioCall];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruption s (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
