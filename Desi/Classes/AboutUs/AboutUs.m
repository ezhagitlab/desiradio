//
//  AboutUs.m
//  Desi
//
//  Created by Jai on 18/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "AboutUs.h"
#import "CommonMacro.h"

@interface AboutUs ()

@property(nonatomic, weak) IBOutlet UILabel *welcomeTitle;
@property(nonatomic, weak) IBOutlet UITextView *detailText;

@end

@implementation AboutUs

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"About Us"];
//    self.navigationItem.backBarButtonItem.tintColor = [UIColor redColor];
    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
//        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"More"
//                                                                     style:UIBarButtonItemStylePlain
//                                                                    target:nil
//                                                                    action:nil];
//        [self.navigationItem setBackBarButtonItem:backItem];
//    }else{
//        //do nothing
//    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
