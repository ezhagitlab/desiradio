//
//  CustomTabBarController.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "CustomTabBarController.h"

#import "MoreVC.h"
#import "RadioVC.h"
#import "EventsVC.h"
#import "NewsVC.h"
#import "RequestVC.h"
#import "Utils.h"
#import "Constants.h"
#import "Logger.h"

@interface CustomTabBarController ()

@end

@implementation CustomTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NewsVC *newsVC = [[NewsVC alloc]initWithNibName:@"NewsVC" bundle:nil];
    newsVC.isNews = YES;
    UINavigationController *newsNav = [[UINavigationController alloc]initWithRootViewController:newsVC];

    EventsVC *eventsVC = [[EventsVC alloc]initWithNibName:@"EventsVC" bundle:nil];
    UINavigationController *eventsNav = [[UINavigationController alloc]initWithRootViewController:eventsVC];

    RadioVC *dashVC = nil;
    if([Utils isLargeScreen])
        dashVC = [[RadioVC alloc]initWithNibName:@"RadioVCiPhone5" bundle:nil];
    else
        dashVC = [[RadioVC alloc]initWithNibName:@"RadioVC" bundle:nil];

    UINavigationController *dashNav = [[UINavigationController alloc]initWithRootViewController:dashVC];

    RequestVC *requestVC;
    
    if([Utils isLargeScreen])
        requestVC = [[RequestVC alloc]initWithNibName:@"RequestVCiPhone5" bundle:nil];
    else
        requestVC = [[RequestVC alloc]initWithNibName:@"RequestVC" bundle:nil];
        
    UINavigationController *requestNav = [[UINavigationController alloc]initWithRootViewController:requestVC];

    MoreVC *moreVC = nil;
    if([Utils isLargeScreen])
        moreVC = [[MoreVC alloc]initWithNibName:@"MoreVCiPhone5" bundle:nil];
    else
        moreVC = [[MoreVC alloc]initWithNibName:@"MoreVC" bundle:nil];
    

    
    UINavigationController *moreNav = [[UINavigationController alloc]initWithRootViewController:moreVC];

    self.viewControllers = [NSArray arrayWithObjects:newsNav, eventsNav, dashNav, requestNav, moreNav, nil];
    
    TRC_DBG(@"Tab bar height %f", self.tabBar.frame.size.height);

//  [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBarBack.jpg"] forBarMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setTintColor:[UIColor redColor]];
    
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                           font, NSFontAttributeName,
                                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                           nil]];
        
    CGRect frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 2);
    UIView *v = [[UIView alloc] initWithFrame:frame];
    [v setBackgroundColor:[UIColor redColor]];
    [[self tabBar] addSubview:v];
    
    [self setSelectedIndex:2];
}

-(void)handlePushForActioId:(NSInteger)actioID
{
    switch (actioID) {
            
        case 1:
            [self setSelectedIndex:0];
            break;

        case 2:
            [self setSelectedIndex:4];
            [[NSNotificationCenter defaultCenter] postNotificationName:kEventsNotificationReceived object:nil];
            break;

        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
