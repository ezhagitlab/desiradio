//
//  NewsVC.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "NewsVC.h"
#import "Utils.h"
#import "EnumConstants.h"
#import "WebServiceManager.h"
#import "NewsAndResponseCollection.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NewsCell.h"
#import "NewsEventsResponse.h"
#import "Logger.h"

@interface NewsVC ()

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NewsAndResponseCollection *newsCollections;
@property(nonatomic, strong) NSMutableArray *tableData;

@end

@implementation NewsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemNews];
    
    return self;
}

-(void)callNewsEventsWebService
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[WebServiceManager sharedInstance] getNewsAndEventsRequestHandler:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        self.newsCollections = [[NewsAndResponseCollection alloc]initWithObject:responseObject];
        self.tableData = (self.isNews) ? self.newsCollections.newsArray : self.newsCollections.eventsArray;
        
        if(!self.tableData.count)
        {
            NSString *alertMsg = (self.isNews) ? @"Currently no news available": @"Currently no events available";
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView setDelegate:self];
            [alertView show];
            return;
        }
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *err)
    {
        TRC_DBG(@"Failure: %@", err.description);
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:@"No Internet Connectivity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView setDelegate:self];
        [alertView show];
        return;

    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(self.isNews)
    {
        [self.tabBarController setSelectedIndex:2];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES]	;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self callNewsEventsWebService];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView setHidden:YES];
    
    [self.navigationItem setTitle:(self.isNews) ? @"News" : @"Events"];
    
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    TRC_DBG(@"Table rows %d", self.tableData.count);
    return self.tableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *newsCellID = @"newsCell";
    
    NewsCell *newsCell = [tableView dequeueReusableCellWithIdentifier:newsCellID];
   
    if(newsCell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:self options:nil];
        newsCell = [nib objectAtIndex:0];
        [newsCell.btn addTarget:self action:@selector(btnTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [newsCell.btn setTag:indexPath.row];
    
    NewsEventsResponse *newsEvents = [self.tableData objectAtIndex:indexPath.row];
    [newsCell.newsTextView setText:newsEvents.title];
    [newsCell.dateLabel setText:newsEvents.date.description];
    
    return newsCell;
}

-(void)btnTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    NSInteger index = btn.tag;
    
    NewsEventsResponse *newsEvent = [self.tableData objectAtIndex:index];
    TRC_DBG(@"URL STRING %@", newsEvent.link);
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:newsEvent.link]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newsEvent.link]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    TRC_DBG(@"Selected row %d", indexPath.row);
    NewsEventsResponse *newsEvent = [self.tableData objectAtIndex:indexPath.row];
    TRC_DBG(@"URL STRING %@", newsEvent.link);

    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:newsEvent.link]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newsEvent.link]];
    }
}
@end
