//
//  NewsCell.h
//  Desi
//
//  Created by Jai on 02/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UITextView *newsTextView;
@property(nonatomic, weak) IBOutlet UILabel *dateLabel;
@property(nonatomic, weak) IBOutlet UIButton *btn;

@end
