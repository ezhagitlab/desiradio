//
//  RequestDummyVC.m
//  Desi
//
//  Created by Jai on 17/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "RequestDummyVC.h"

@interface RequestDummyVC ()

@end

@implementation RequestDummyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
