//
//  NetworkFinder.h
//  Desi
//
//  Created by Jai on 12/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkFinder : NSObject

+(id)sharedInstance;
-(BOOL)isNetworkAvailable;

@end
