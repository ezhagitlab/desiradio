//
//  NetworkFinder.m
//  Desi
//
//  Created by Jai on 12/04/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "NetworkFinder.h"
#import "Reachability.h"
#import "Logger.h"

@interface NetworkFinder()
@property(nonatomic, strong) Reachability *reachability;
@end

@implementation NetworkFinder


+(id)sharedInstance
{
    static NetworkFinder *sharedInstance = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedInstance = [[self alloc ]init];
    });
    return sharedInstance;
}

-(Reachability*)reachability
{
    if(!_reachability)
    {
//        _reachability = [Reachability reachabilityForInternetConnection];
        _reachability = [Reachability reachabilityWithHostName:@"www.apple.com"];
        if([_reachability startNotifier])
            TRC_DBG(@"Reachability notifier has started successfully");
    }
    return _reachability;
}

-(BOOL)isNetworkAvailable
{
 
    NetworkStatus networkStatus = [self.reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        TRC_DBG(@"There is NO internet connection");
        return NO;
    }
    else
    {
        TRC_DBG(@"There is internet connection");
    }
    return YES;
}


@end
