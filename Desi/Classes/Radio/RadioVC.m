//
//  DashBoard.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "RadioVC.h"
#import "Utils.h"
#import "SplashVC.h"
#import "Visualizer.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "NetworkFinder.h"
#import "Constants.h"
#import "Logger.h"

@interface RadioVC ()

@property (nonatomic, weak) IBOutlet UIButton *playPauseBtn;
@property(nonatomic) BOOL isPlaying, isPaused;
@property(nonatomic, strong) IBOutlet Visualizer *visualizer;
@property(nonatomic, weak) IBOutlet UITextView *songTitleView;
@property(nonatomic, strong) NSMutableData *receivedData;
@property(nonatomic) BOOL isDeviceTokenSent;


-(IBAction)playPauseBtnPressed:(id)sender;
@end

static const CGFloat VISUALIZER_WIDTH  = 310;
static const CGFloat VISUALIZER_HEIGHT = 200;

static void *AVPlayerDemoPlaybackViewControllerRateObservationContext = &AVPlayerDemoPlaybackViewControllerRateObservationContext;
static void *AVPlayerDemoPlaybackViewControllerStatusObservationContext = &AVPlayerDemoPlaybackViewControllerStatusObservationContext;

@implementation RadioVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemRadio];

    return self;
}

-(NSMutableData*)receivedData{
    if(!_receivedData){
        _receivedData = [[NSMutableData alloc]init];
    }
    return _receivedData;
}

-(void)checkPlayerIsPlaying:(NSTimer*)timer{
    TRC_DBG(@"%i", self.player.status);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
            /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerStatusUnknown:
            {

                TRC_DBG(@"AVPlayerStatusUnknown");
                [self disablePlayPauseButton];
            }
                break;
                
            case AVPlayerStatusReadyToPlay:
            {

                TRC_DBG(@"AVPlayerStatusReadyToPlay");
                [self enablePlayPauseButton];
            }
                break;
                
            case AVPlayerStatusFailed:
            {
                TRC_DBG(@"AVPlayerStatusFailed");
            }
                break;
        }
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {

    TRC_DBG(@"End Interruption with flags called");
}

-(void)lostInteruptNotification:(NSNotification*)notification{
    TRC_DBG(@"Interruption notification name %@", notification.name);
    
}

-(void)restInteruptNotification:(NSNotification*)notification{
    TRC_DBG(@"Interruption notification name %@", notification.name);
    
}

-(void)routeChangeNotification:(NSNotification*)notification{
    TRC_DBG(@"Interruption notification name %@", notification.name);
    
}

- (void) onAudioSessionEvent: (NSNotification *) notification
{
    //Check the type of notification, especially if you are sending multiple AVAudioSession events here
    TRC_DBG(@"Interruption notification name %@", notification.name);
    if ([notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        TRC_DBG(@"Interruption notification received %@!", notification);
        
        //Check to see if it was a Begin interruption
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeBegan]]) {
            TRC_DBG(@"Interruption began!");
            
        } else if([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeEnded]]){

            TRC_DBG(@"Player status %i", self.player.status);
            // Resume playing the audio.
            [self.player play];
            
        }else{
            TRC_DBG(@"Something happened");
        }
    }else{
        TRC_DBG(@"Not specified type of interuption");
    }
}

-(void)activateAudioSession
{
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    
    BOOL isAudioSessionActivted = [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    if(!isAudioSessionActivted)
        TRC_DBG(@"Failed to start audio session");
}

-(void)removeObservers
{
    [self.player removeObserver:self forKeyPath:@"status"];
}

-(void)createAudioPlayerAndPlay
{
    [self removeObservers];
    
    self.player = nil;
    self.player = [[AVQueuePlayer alloc] initWithURL:[NSURL URLWithString:@"http://s5.voscast.com:8040/;stream.mp3"]];
    [self.player play];
    self.isPlaying = YES;
    self.isPaused  = NO;
    
    [self.player addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionPrior | NSKeyValueObservingOptionOld context:nil];
    
}

-(void)addObserversForAudioSession
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioSessionEvent:) name:AVAudioSessionInterruptionNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restInteruptNotification:) name:AVAudioSessionMediaServicesWereResetNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeChangeNotification:) name:AVAudioSessionRouteChangeNotification object:nil];
}

-(void)dismissSplash{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self getSongTitles:nil];
    [self startRadio];
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];

}

-(void)getSongTitles:(NSTimer*)timer{
    
    [[WebServiceManager sharedInstance] getSongTitleRequestHandler:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        [self setSongTitleForAlbum:string];
        
/*        if(self.isPlaying)
        {
            [self changeToPauseBtnImage];
            [self startRadio];
        }
*/        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *err)
    {
        TRC_DBG(@"Error: %@", err.description);
    }];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
   
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *responeString = [[NSString alloc] initWithData:self.receivedData
                                                    encoding:NSUTF8StringEncoding];
    TRC_DBG(@"Response string %@", responeString);
    
    [self setSongTitleForAlbum:responeString];
}

-(void)setSongTitleForAlbum:(NSString*)htmlString{

    NSString *title = [self parseString:htmlString];
    self.songTitleView.text = (title && ![@"" isEqualToString:title]) ? title : self.songTitleView.text;
}

-(NSString*)parseString:(NSString*)htmlString{
    
    NSRange startRange  = [htmlString rangeOfString:@"innerHTML = '"];
    NSRange endRange    = [htmlString rangeOfString:@"';"];
    
    TRC_DBG(@"Start location %i", startRange.location);
    TRC_DBG(@"Length %i", startRange.length);
    
    if(startRange.location == NSNotFound || endRange.location == NSNotFound)
    {
        TRC_DBG(@"Start range or end range not found");
        return @"";
    }
    
    if(endRange.location == NSNotFound || endRange.location == NSNotFound)
    {
        TRC_DBG(@"End range or end range not found");
        return @"";
    }
    
    NSUInteger location = startRange.location + startRange.length;
    NSUInteger length   = endRange.location - location;
    
    if((location + length) > (htmlString.length - 1))
    {
        TRC_DBG(@"Location length is out of range");
        return @"";
    }
    NSRange rangeForSongTitle = NSMakeRange(location, length);
    NSString *title = [htmlString substringWithRange:rangeForSongTitle];
    
    TRC_DBG(@"Song title %@", title);
    return title;
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    TRC_DBG(@"Connection Failed");
    return;
}

-(void)changeToPlayBtnImage
{
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"button_play.png"] forState:UIControlStateNormal];
}

-(void)startRadio{
    
    [self activateAudioSession];
    
    [self createAudioPlayerAndPlay];
    
    [self addObserversForAudioSession];
    
    TRC_DBG(@"Player started");
    [self.navigationItem setTitle:@"Live Radio"];
    
}

-(void)changeToPauseBtnImage
{
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];
}

-(void)pauseRadio
{
    [self.player pause];
    self.isPaused  = YES;
    self.isPlaying = NO;
}

-(IBAction)playPauseBtnPressed:(id)sender{

    TRC_DBG(@"Av player status %d", self.player.status);
    
    if(self.isPlaying)
    {
        TRC_DBG(@"Radio paused");
        [self pauseRadio];
        [self changeToPlayBtnImage];
    }
    else if(self.isPaused)
    {
        TRC_DBG(@"Radio played");
        [self startRadio];
        [self changeToPauseBtnImage];
    }
}

- (void)beginInterruption

{ /* something has caused your audio session to be interrupted */
    TRC_DBG(@"Begin Interupted");
}

/* Currently the only flag is AVAudioSessionInterruptionFlags_ShouldResume. */
- (void)endInterruption
{
    /* endInterruptionWithFlags: will be called instead if implemented. */
    TRC_DBG(@"End Interupted");
    [self.player play];
}

/* notification for input become available or unavailable */
- (void)inputIsAvailableChanged:(BOOL)isInputAvailable
{
    TRC_DBG(@"Input availabilty changed delegate called");
}

-(void)setFrameToOutlet
{
    //set label frame
    CGFloat screenWidth  = [Utils screenWidth];
    CGFloat screenHeight = [Utils screenHeight];
    CGFloat btnWidth     = self.playPauseBtn.frame.size.width;
    
    CGFloat y = 0 , x = 0;

    x = screenWidth/2 - btnWidth/2;
    if([Utils isLargeScreen])
        y = screenHeight/1.62;
    
    //Play pause button frame
    self.playPauseBtn.frame = CGRectMake(x, y, 60.0, 60.0);
}

/*
-(void)addVisualizer{
    
    CGFloat screenHeight = [Utils screenHeight];
    
    CGFloat y = 0 , x = 10;
    if([Utils isLargeScreen])
        y = screenHeight/2;
    else
        y = screenHeight/3;

}
*/
-(void)startVisualizer{
//    [self.visualizer startVisualizer];
}

-(void)stopVisualizer{
//    [self.visualizer stopVisualizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
}


-(void)addSplashVC{

    SplashVC *splashVC = nil;
    if ([Utils isLargeScreen]) {
        splashVC = [[SplashVC alloc]initWithNibName:@"SplashVCiPhone5" bundle:nil];
        TRC_DBG(@"Large screen is loaded");
    }else{
        splashVC = [[SplashVC alloc]initWithNibName:@"SplashVC" bundle:nil];
        TRC_DBG(@"Small screen is loaded");
    }
    
    [self.navigationController presentViewController:splashVC animated:NO completion:^{
        [self performSelector:@selector(dismissSplash) withObject:self afterDelay:3.0];
    }];
}

-(void)scheduleTheTimer{
    
    [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(getSongTitles:) userInfo:nil repeats:YES];
}


- (void)displayMailComposerSheet
{
    
//    NSString *deviceToken = [[[UIApplication sharedApplication] delegate].dev];

    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *deviceToken = delegate.deviceTokenForPush;
    TRC_DBG(@"Device token %@", deviceToken);
    
    if(![MFMailComposeViewController canSendMail]){
        TRC_DBG(@"Device doesn't not supported for mail");
        return;
    }
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Device token"];
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:(NSString*)DESI_BEATS_EMAIL];
    
	[picker setToRecipients:toRecipients];
	[picker setMessageBody:deviceToken isHTML:NO];
	
	[self presentViewController:picker animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    NSString *alertMsg = nil;
    NSString *title = @"Thank You!";
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
            alertMsg = @"Mail cancelled";
            break;
		case MFMailComposeResultSaved:
			alertMsg = @"Mail saved";
			break;
		case MFMailComposeResultSent:
			alertMsg = @"Your song request has been received. Will be played live, keep listening DesiBeatRadio.com.au";
			break;
		case MFMailComposeResultFailed:
            title    = @"Sorry";
			alertMsg = @"Mail sending failed";
			break;
		default:
			title    = @"Sorry";
            alertMsg = @"Result: Mail not sent";
			break;
	}
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)configureForNetworkStatus:(NetworkStatus)status
{
	// do something with status
	switch (status)
	{
		case NotReachable:
			TRC_DBG(@"Not Reachable");
            [self disablePlayPauseButton];
			break;
		case ReachableViaWiFi:
			TRC_DBG(@"Reachable via WiFi");
			[self startRadio];
            [self enablePlayPauseButton];
            break;
		case ReachableViaWWAN:
			TRC_DBG(@"Reachable via WWAN");
            [self startRadio];
            [self enablePlayPauseButton];
			break;
		default:
			break;
	}
}

-(void)reachabilityStatusChanged:(NSNotification*)notification
{
    TRC_DBG(@"Reachability %@", notification);
    
    // get Reachability instance from notification
	Reachability* curReach = [notification object];
    
	// assert that we actually got Reachability instance
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
	// get current status
	NetworkStatus siteNetworkStatus = [curReach currentReachabilityStatus];
    
	// do something with it
	[self configureForNetworkStatus:siteNetworkStatus];
    
}

-(void)registerForRechabilityStatus
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityStatusChanged:) name:kReachabilityChangedNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //Create splash vc and show
    [self addSplashVC];
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];
    
    [self registerForRechabilityStatus];
    [[NetworkFinder sharedInstance] isNetworkAvailable];

    [self scheduleTheTimer];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*if(!self.isDeviceTokenSent)
    {
        [self displayMailComposerSheet];
        self.isDeviceTokenSent = YES;
    }*/
}

-(void)enablePlayPauseButton{

    [self.playPauseBtn setAlpha:1.0];
    [self.playPauseBtn setUserInteractionEnabled:YES];
}

-(void)disablePlayPauseButton{

    [self.playPauseBtn setAlpha:0.5];
    [self.playPauseBtn setUserInteractionEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
