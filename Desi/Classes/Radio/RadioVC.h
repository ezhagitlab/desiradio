//
//  DashBoard.h
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioSession.h>

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface RadioVC : UIViewController<AVAudioSessionDelegate, MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) AVQueuePlayer *player;

@end
