//
//  Visualizer.m
//  TabHoldandRecord
//
//  Created by aram on 3/19/14.
//  Copyright (c) 2014 Ashish Tripathi. All rights reserved.
//

#import "Visualizer.h"

static const float kBarWdith  = 0.5;
static const float gap        = 0.5;

@interface Visualizer()
    
@property(nonatomic, strong)NSTimer *visualtimer;

@end

@implementation Visualizer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        [self addBars];
    }
    return self;
}

-(void)updateVisualizer:(NSTimer*)timer{
    [self addBars];
}

-(void)startVisualizer{

    self.visualtimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(updateVisualizer:) userInfo:nil repeats:YES];
}

-(void)stopVisualizer{
    
    [self.visualtimer invalidate];
    [self removeAllSubviews];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)removeAllSubviews{
    
    for(UIView *subView in self.subviews){
        [subView removeFromSuperview];
    }
}

-(void)addBars{
    
    [self removeAllSubviews];
    float x = self.frame.origin.x;
    float viewHeight = self.frame.size.height;
    
    self.backgroundColor = [UIColor blackColor];
    
    for (int i = 0; x < self.frame.size.width - 1; i++) {
        
        int randomBarHeight =  rand() % ((int)viewHeight);
        float y = viewHeight/2 - randomBarHeight/2;

        UILabel *bar = [[UILabel alloc]initWithFrame:CGRectMake(x, y, kBarWdith, randomBarHeight)];
        x = i*(kBarWdith + gap);
        bar.backgroundColor = [UIColor redColor];
        [self addSubview:bar];
    }
}

@end
