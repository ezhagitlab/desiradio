//
//  Visualizer.h
//  TabHoldandRecord
//
//  Created by aram on 3/19/14.
//  Copyright (c) 2014 Ashish Tripathi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Visualizer : UIView

-(void)startVisualizer;
-(void)stopVisualizer;

@end
