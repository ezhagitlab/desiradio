//
//  WriteUsVC.m
//  Desi
//
//  Created by Jai on 19/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "WriteUsVC.h"
#import "Utils.h"
#import "CommonMacro.h"
#import "WebServiceManager.h"
#import "NetworkFinder.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Logger.h"

@interface WriteUsVC ()

@property(nonatomic, weak) IBOutlet UITextField *yourName, *mobile, *email;
@property(nonatomic, weak) IBOutlet UITextView *feedBack;
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

-(IBAction)submitButtonPressed:(id)sender;
@end

@implementation WriteUsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.scrollView setContentSize:self.view.bounds.size];
    
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Feedback"];
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    self.yourName.borderStyle=UITextBorderStyleNone;
    self.yourName.layer.masksToBounds=YES;
    self.yourName.backgroundColor=[UIColor clearColor];
    self.yourName.layer.borderColor=[[UIColor redColor]CGColor];
    self.yourName.layer.borderWidth= 1.0f;
    
    self.mobile.borderStyle=UITextBorderStyleNone;
    self.mobile.layer.masksToBounds=YES;
    self.mobile.backgroundColor=[UIColor clearColor];
    self.mobile.layer.borderColor=[[UIColor redColor]CGColor];
    self.mobile.layer.borderWidth= 1.0f;
    
    self.email.borderStyle=UITextBorderStyleNone;
    self.email.layer.masksToBounds=YES;
    self.email.backgroundColor=[UIColor clearColor];
    self.email.layer.borderColor=[[UIColor redColor]CGColor];
    self.email.layer.borderWidth= 1.0f;
    
    self.feedBack.layer.masksToBounds=YES;
    self.feedBack.backgroundColor=[UIColor clearColor];
    self.feedBack.layer.borderColor=[[UIColor redColor]CGColor];
    self.feedBack.layer.borderWidth= 1.0f;
    
    [self setAttributedPlaceHolder:self.yourName withPlaceHolederText:@"Name"];
    [self setAttributedPlaceHolder:self.mobile withPlaceHolederText:@"Mobile"];
    [self setAttributedPlaceHolder:self.email withPlaceHolederText:@"Email"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBeginTextViewChanged:) name:UITextViewTextDidBeginEditingNotification object:nil];
}

-(void)didBeginTextViewChanged:(NSNotification*)notification{
    
//    self.activeTextField = nil;
//    [[NSNotificationCenter defaultCenter]postNotificationName:UIKeyboardDidShowNotification object:nil];
    if([self.feedBack.text isEqualToString:@"Write your feed back here"]){
        self.feedBack.text = @"";
    }
}

-(void)setAttributedPlaceHolder:(UITextField*)textField withPlaceHolederText:(NSString*)placeHolderText{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolderText attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        TRC_DBG(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
        textField.text = placeHolderText;
    }
}

-(NSString*)validationAlertMsg{
    
    NSString *alertMsg = @"";
    
    TRC_DBG(@"Name text field %@", self.yourName.text);
    if(!self.yourName.text || [@"" isEqualToString:self.yourName.text]){
        
        alertMsg = @"Enter your name";
        
    }else if(!self.mobile.text || [@"" isEqualToString:self.mobile.text] || ![Utils mobileNumberValidation:self.mobile.text]){
        
        alertMsg     = @"Enter valid mobile number";
    }else if (!self.email.text || [@"" isEqualToString:self.email.text] || ![Utils emailValidation:self.email.text]){
        
        alertMsg     = @"Enter valid email id";
    }else if (!self.feedBack.text || [@"" isEqualToString:self.feedBack.text] || [@"Write your feed back here" isEqualToString:self.feedBack.text]){
        
        alertMsg     = @"Enter your feed back";
    }
    return alertMsg;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

- (void)displayMailComposerSheet
{
    
    if(![MFMailComposeViewController canSendMail]){
        TRC_DBG(@"Device doesn't not supported for mail");
        return;
    }
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Feed back"];
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:(NSString*)DESI_BEATS_EMAIL];

	[picker setToRecipients:toRecipients];
    
	// Fill out the email body text
	NSString *emailBody = [NSString stringWithFormat:@"Your name: %@\n Mobile number: %@\n Email id: %@\n Message: %@", self.yourName.text, self.mobile.text, self.email.text, self.feedBack.text];
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self.navigationController presentViewController:picker animated:YES completion:NULL];
}

-(void)sendEmail
{
    
	NSString *emailBody = [NSString stringWithFormat:@"Your name: %@\n Mobile number: %@\n Email id: %@\n Message: %@", self.yourName.text, self.mobile.text, self.email.text, self.feedBack.text];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[WebServiceManager sharedInstance] sendEmailInBackground:emailBody isSongRequest:NO clearTextFieldsBlock:^{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.yourName setText:@""];
        [self.mobile setText:@""];
        [self.email setText:@""];
        [self.feedBack setText:@""];
    }];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    NSString *alertMsg = nil;
    NSString *title = @"Thank You!";
    
	//Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
            alertMsg = @"Mail cancelled";
            break;
		case MFMailComposeResultSaved:
			alertMsg = @"Mail saved";
			break;
		case MFMailComposeResultSent:
			alertMsg = @"Thank you for your valuable feed back, keep listening DesiBeatRadio.com.au";
			break;
		case MFMailComposeResultFailed:
            title    = @"Sorry";
			alertMsg = @"Mail sending failed";
			break;
		default:
			title    = @"Sorry";
            alertMsg = @"Result: Mail not sent";
			break;
	}
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.tabBarController setSelectedIndex:2];
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          font, NSFontAttributeName,
                                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                                          nil]];
    
	[self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    
}

-(IBAction)submitButtonPressed:(id)sender{
    
    if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
        [Utils showNoInterConnectivityMessage];
        return;
    }
    
    NSString *validationMsg = [self validationAlertMsg];
    if(validationMsg)
        TRC_DBG(@"Validation string %@", validationMsg);
    if(![@"" isEqualToString:validationMsg]){
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:validationMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIColor *titleColor = nil;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        titleColor = [UIColor colorWithRed:(0.0/255.0) green:(122.0/255.0) blue:(255.0/255.0) alpha:1.0];
    else
        titleColor = [UIColor whiteColor];
    
    UIFont *font = [UIFont systemFontOfSize:20];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          font, NSFontAttributeName,
                                                          titleColor, NSForegroundColorAttributeName,
                                                          nil]];
    
//  [self displayMailComposerSheet];
    [self sendEmail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
