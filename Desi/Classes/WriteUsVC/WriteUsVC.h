//
//  WriteUsVC.h
//  Desi
//
//  Created by Jai on 19/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface WriteUsVC : UIViewController<MFMailComposeViewControllerDelegate>

@end
