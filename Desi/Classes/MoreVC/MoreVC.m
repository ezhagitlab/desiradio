//
//  MoreVC.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "MoreVC.h"
#import "Utils.h"
#import "AboutUs.h"
#import "WriteUsVC.h"
#import "EventsVC.h"
#import "NewsVC.h"
#import "Constants.h"
#import "NetworkFinder.h"
#import "Logger.h"

@interface MoreVC ()

@property(nonatomic, strong) IBOutlet UITableView *moreTableView;
@property(nonatomic, strong) NSArray *tableArray;
@property(nonatomic, strong) AboutUs *aboutUS;
@property(nonatomic, strong) WriteUsVC *writeUS;
@end

@implementation MoreVC

-(NSArray*)tableArray{
    if(!_tableArray){
        _tableArray = [NSArray arrayWithObjects:@"Events", @"Feedback", @"Like Us", @"Tweet Us", @"Official Website ", @"About Us", nil];
    }
    return _tableArray;
}

-(AboutUs*)aboutUS{
    
    if(!_aboutUS)
        _aboutUS = [[AboutUs alloc]init];
    return _aboutUS;
}

-(WriteUsVC*)writeUS{
    
    if(!_writeUS){
        if ([Utils isLargeScreen]) {
            _writeUS = [[WriteUsVC alloc]initWithNibName:@"WriteUsVCiPhone5" bundle:nil];

        }else{
            _writeUS = [[WriteUsVC alloc]initWithNibName:@"WriteUsVC" bundle:nil];
        }
    }
    return _writeUS;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived:) name:(NSString*)kEventsNotificationReceived object:nil];
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemMore];

    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    TRC_DBG(@"Number of rows %d", self.tableArray.count);
    return self.tableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellId = @"More Table Cell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(cell == nil){

        TRC_DBG(@"Cell created");
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell.textLabel setTextColor:[UIColor redColor]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = [self.tableArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        TRC_DBG(@"Cell reused");
    }
    return cell;
}

- (void)makeSkypeAudioCall
{
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]];
    
    if(installed)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"skype:desibeatradio.org?call"]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.com/apps/skype/skype"]];
    }
}

-(void)makeAudioCall{
    
    NSString *phNo = @"tel://123";
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phNo]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phNo]];
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:@"This device doesn't support" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

-(void)pushNewsViewController
{
    NewsVC *newsVC = [[NewsVC alloc]initWithNibName:@"NewsVC" bundle:nil];
    [self.navigationController pushViewController:newsVC    animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRC_DBG(@"Selected row %d", indexPath.row);

    switch (indexPath.row) {
            
        case DesiBeatsCallUs:{
//            [self makeSkypeAudioCall];
            [self pushNewsViewController];
        }break;

        case DesiBeatsWriteUs:
            if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
                [Utils showNoInterConnectivityMessage];
                return;
            }
            [self.navigationController pushViewController:self.writeUS animated:YES];
            break;
            
        case DesiBeatsLikeUs:
        {
            if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
                [Utils showNoInterConnectivityMessage];
                return;
            }
            
            NSURL *facebookURL = [NSURL URLWithString: @"https://www.facebook.com/desibeatradio.fanpage"];
            NSURL *facebookAppURL = [NSURL URLWithString: @"fb://profile/263409447095514"];

            if([[UIApplication sharedApplication] canOpenURL:facebookAppURL])
            {
                [[UIApplication sharedApplication] openURL:facebookAppURL];
            }
            else
            {
                [[UIApplication sharedApplication] openURL:facebookURL];
            }
        }
            break;
            
        case DesiBeatsTweetUs:
        {
            if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
                [Utils showNoInterConnectivityMessage];
                return;
            }
            
            NSURL *twitterURL = [NSURL URLWithString:@"https://twitter.com/dbrfanpage"];
            if([[UIApplication sharedApplication] canOpenURL:twitterURL])
            {
                [[UIApplication sharedApplication] openURL:twitterURL];
            }
            else
            {
                TRC_DBG(@"Can't open URL:%@", twitterURL);
            }
        }
            break;
            

        case DesiBeatsVisitUs:
        {

            if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
                [Utils showNoInterConnectivityMessage];
                return;
            }
            
            NSURL *officialURL = [NSURL URLWithString:@"http://www.desibeatradio.org/"];
            if([[UIApplication sharedApplication] canOpenURL:officialURL])
            {
                [[UIApplication sharedApplication] openURL:officialURL];
            }
            else
            {
                TRC_DBG(@"Can't open URL:%@", officialURL);
            }
        }
            break;
            
        case DesiBeatsAboutUs:
            [self.navigationController pushViewController:self.aboutUS animated:YES];
            break;
            
        default:
            break;
    }
}

-(void)pushNotificationReceived:(NSNotification*)notification
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self pushNewsViewController];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationItem setTitle:@"More"];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName ,nil] forState:UIControlStateNormal];
    //set back button arrow color
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    //Register for notification push
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
