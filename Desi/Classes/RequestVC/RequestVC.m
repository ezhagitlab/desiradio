//
//  RequestVCViewController.m
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "RequestVC.h"
#import "EnumConstants.h"
#import "Utils.h"
#import "CommonMacro.h"
#import "WebServiceManager.h"
#import "NetworkFinder.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Logger.h"

@interface RequestVC ()

@property(nonatomic, weak) IBOutlet UITextField *yourName, *singerName, *songName;
@property(nonatomic, weak) IBOutlet UITextView *message;
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property(nonatomic, weak) UITextField *activeTextField;
//@property(nonatomic, weak) IBOutlet UITextView *activeTextView;

-(IBAction)submitButtonPressed:(id)sender;
@end

@implementation RequestVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemRequest];

    return self;
}

- (void)displayMailComposerSheet
{

    if(![MFMailComposeViewController canSendMail]){
        TRC_DBG(@"Device doesn't not supported for mail");
        return;
    }
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Song Request"];
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:(NSString*)DESI_BEATS_EMAIL];

	[picker setToRecipients:toRecipients];

	// Fill out the email body text
	NSString *emailBody = [NSString stringWithFormat:@"Your name: %@\n Singer name: %@\n Song name: %@\n Message: %@", self.yourName.text, self.singerName.text, self.songName.text, self.message.text];
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self.navigationController presentViewController:picker animated:YES completion:NULL];
}

-(void)sendEmail
{
    
    NSString *emailBody = [NSString stringWithFormat:@"Your name: %@\n Singer name: %@\n Song name: %@\n Message: %@", self.yourName.text, self.singerName.text, self.songName.text, self.message.text];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[WebServiceManager sharedInstance] sendEmailInBackground:emailBody isSongRequest:YES clearTextFieldsBlock:^{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.yourName setText:@""];
        [self.singerName setText:@""];
        [self.songName setText:@""];
        [self.message setText:@""];
    }];

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{

    NSString *alertMsg = nil;
    NSString *title = @"Thank You!";
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
            alertMsg = @"Mail cancelled";
            break;
		case MFMailComposeResultSaved:
			alertMsg = @"Mail saved";
			break;
		case MFMailComposeResultSent:
			alertMsg = @"Your song request has been received. Will be played live, keep listening DesiBeatRadio.com.au";
			break;
		case MFMailComposeResultFailed:
            title    = @"Sorry";
			alertMsg = @"Mail sending failed";
			break;
		default:
			title    = @"Sorry";
            alertMsg = @"Result: Mail not sent";
			break;
	}
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    [self.tabBarController setSelectedIndex:2];
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          font, NSFontAttributeName,
                                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                                          nil]];

	[self.navigationController dismissViewControllerAnimated:YES completion:NULL];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:NO];
}

-(NSString*)validationAlertMsg{
    
    NSString *alertMsg = @"";
    
    if(!self.yourName.text || [@"" isEqualToString:self.yourName.text]){
        
        alertMsg = @"Enter your name";
        
    }else if(!self.singerName.text || [@"" isEqualToString:self.singerName.text]){

        alertMsg     = @"Enter singer name";
    }else if (!self.songName.text || [@"" isEqualToString:self.songName.text]){
       
        alertMsg     = @"Enter song name";
    }else if (!self.message.text || [@"" isEqualToString:self.message.text] || [@"Enter your message here..." isEqualToString:self.message.text]){
        
        alertMsg     = @"Enter the message";
    }
    return alertMsg;
}

-(IBAction)submitButtonPressed:(id)sender{
    
    if(![[NetworkFinder sharedInstance] isNetworkAvailable]){
        [Utils showNoInterConnectivityMessage];
        return;
    }
    
    NSString *validationMsg = [self validationAlertMsg];
    if(validationMsg)
        TRC_DBG(@"Validation string %@", validationMsg);
    if(![@"" isEqualToString:validationMsg]){
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:validationMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIColor *titleColor = nil;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        titleColor = [UIColor colorWithRed:(0.0/255.0) green:(122.0/255.0) blue:(255.0/255.0) alpha:1.0];
    else
        titleColor = [UIColor whiteColor];
    
     UIFont *font = [UIFont systemFontOfSize:20];
     [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
     font, NSFontAttributeName,
     titleColor, NSForegroundColorAttributeName,
     nil]];
     
//     [self displayMailComposerSheet];

    [self sendEmail];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self registerForKeyboardNotifications];
    
    [self.scrollView setContentSize:self.view.bounds.size];
    
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Song Request"];
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];


    self.yourName.borderStyle=UITextBorderStyleNone;
    self.yourName.layer.masksToBounds=YES;
    self.yourName.backgroundColor=[UIColor clearColor];
    self.yourName.layer.borderColor=[[UIColor redColor]CGColor];
    self.yourName.layer.borderWidth= 1.0f;

    self.singerName.borderStyle=UITextBorderStyleNone;
    self.singerName.layer.masksToBounds=YES;
    self.singerName.backgroundColor=[UIColor clearColor];
    self.singerName.layer.borderColor=[[UIColor redColor]CGColor];
    self.singerName.layer.borderWidth= 1.0f;

    self.songName.borderStyle=UITextBorderStyleNone;
    self.songName.layer.masksToBounds=YES;
    self.songName.backgroundColor=[UIColor clearColor];
    self.songName.layer.borderColor=[[UIColor redColor]CGColor];
    self.songName.layer.borderWidth= 1.0f;

    self.message.layer.masksToBounds=YES;
    self.message.backgroundColor=[UIColor clearColor];
    self.message.layer.borderColor=[[UIColor redColor]CGColor];
    self.message.layer.borderWidth= 1.0f;
    
    [self setAttributedPlaceHolder:self.yourName withPlaceHolederText:@"Your name"];
    [self setAttributedPlaceHolder:self.singerName withPlaceHolederText:@"Singer's name"];
    [self setAttributedPlaceHolder:self.songName withPlaceHolederText:@"Song name"];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBeginTextViewChanged:) name:UITextViewTextDidBeginEditingNotification object:nil];
    
}

-(void)setAttributedPlaceHolder:(UITextField*)textField withPlaceHolederText:(NSString*)placeHolderText{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolderText attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        TRC_DBG(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
        textField.text = placeHolderText;
    }
    
}

-(void)didBeginTextViewChanged:(NSNotification*)notification{
    
    self.activeTextField = nil;
    [[NSNotificationCenter defaultCenter]postNotificationName:UIKeyboardDidShowNotification object:nil];
    if([self.message.text isEqualToString:@"Enter your message here..."]){
        self.message.text = @"";
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.activeTextField = nil;
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if(self.activeTextField)
        return;
    
    TRC_DBG(@"Key board was shown");
    NSDictionary* info = [aNotification userInfo];
    TRC_DBG(@"Info dic %@", info);
    
    CGSize kbSize;
    if(info)
        kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    else
        kbSize = CGSizeMake(320, 216);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.message.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.message.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    TRC_DBG(@"Key board will be hidden");
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
