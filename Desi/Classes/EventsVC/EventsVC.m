//
//  EventsVC.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "EventsVC.h"
#import "Utils.h"
#import "CommonMacro.h"
#import "Logger.h"

@interface EventsVC ()

@end

@implementation EventsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemEvents];
    
    //set custom back button
    
  /*  UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    
    [btnAdd setContentMode:UIViewContentModeScaleAspectFit];
    
    [btnAdd setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    [btnAdd addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backbarBtn = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    
    [self.navigationItem setBackBarButtonItem:backbarBtn];*/
    
    return self;
}

-(void)backButtonPressed:(id)sender{
    
    TRC_DBG(@"Back bat button pressed");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationItem setTitle:@"Events"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
