//
//  CommonMacro.h
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#ifndef Desi_CommonMacro_h
#define Desi_CommonMacro_h

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#endif
