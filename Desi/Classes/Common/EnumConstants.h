//
//  EnumConstants.h
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#ifndef Desi_EnumConstants_h
#define Desi_EnumConstants_h


typedef enum DesiBeatsTabBarItem
{
    DesiBeatsItemNews,
    DesiBeatsItemEvents,
    DesiBeatsItemRadio,
    DesiBeatsItemRequest,
    DesiBeatsItemMore

}DesiBeatsTabBarItem;

typedef enum MoreVCItems
{
    DesiBeatsCallUs,
    DesiBeatsWriteUs,
    DesiBeatsLikeUs,
    DesiBeatsTweetUs,
    DesiBeatsVisitUs,
    DesiBeatsAboutUs
    
}DesiBeatsMoreScreenItems;

#endif
