//
//  Constants.h
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const DesiBaseURL;
extern NSString *const kEventsNotificationReceived;

extern const NSInteger kNewsActionId;
extern const NSInteger kEventsActionId;

extern NSString const *DESI_SONG_TITLE_URL;
extern NSString const *DESI_BEATS_EMAIL;

extern NSString const *eMailSubSongRequest;
extern NSString const *eMailSubFeedBack;

extern NSString const *eMailIdSender;
extern NSString const *eMailIdSenderPwd;

extern NSString const *eMailResponseSongRequest;
extern NSString const *eMailResponseFeedBack;

extern NSString const *kFeedBackFailed;
extern NSString const *kSongRequestFailed;
