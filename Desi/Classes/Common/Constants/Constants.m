//
//  Constants.m
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "Constants.h"

NSString *const DesiBaseURL = @"google.com";
NSString *const kEventsNotificationReceived = @"EventsPush";

const NSInteger  kNewsActionId    = 1;
const NSInteger  kEventsActionId  = 2;

NSString const *DESI_SONG_TITLE_URL     = @"http://cdn.voscast.com/stats/display.js?key=8bb0d97fb338dc652ffc415b33472ecb&stats=songtitle";

NSString const *DESI_BEATS_EMAIL    = @"desibeatradiosongrequest@gmail.com";
//NSString const *DESI_BEATS_EMAIL    = @"brno9jk@gmail.com";

NSString const *eMailSubSongRequest = @"Song Request";
NSString const *eMailSubFeedBack    = @"Feed Back";

//NSString const *eMailIdSender       = @"lucrotechnologies@gmail.com";
//NSString const *eMailIdSenderPwd    = @"qwertyjack";

NSString const *eMailIdSender       = @"sendformrequest@gmail.com";
NSString const *eMailIdSenderPwd    = @"live2014";

NSString const *eMailResponseSongRequest = @"Your song request has been received. Will be played live, keep listening www.desibeatradio.org";
NSString const *eMailResponseFeedBack    = @"Thank you for your valuable feed back, keep listening www.desibeatradio.org";

NSString const *kFeedBackFailed       = @"Failed to send feed back";
NSString const *kSongRequestFailed    = @"Failed to send song request";

