//
//  Utils.h
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EnumConstants.h"

@interface Utils : NSObject

+(BOOL)isLargeScreen;
+(NSString*)titleForTabBarIndex:(DesiBeatsTabBarItem)index;
+(NSString*)imageNameForTabBarItemSelected:(DesiBeatsTabBarItem)index;
+(NSString*)imageNameForTabBarItemUnSelected:(DesiBeatsTabBarItem)index;
+(UITabBarItem*)tabbarButtonItemForIndex:(DesiBeatsTabBarItem)indexForItem;
+(void)showNoInterConnectivityMessage;
+(BOOL)isNetworkAvailable;
+(CGFloat)screenHeight;
+(CGFloat)screenWidth;
+(BOOL)emailValidation:(NSString*)emailid;
+(BOOL)mobileNumberValidation:(NSString*)mobileNumber;

@end
