//
//  Utils.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "Utils.h"
#import "CommonMacro.h"
#import "Reachability.h"
#import "Logger.h"

@implementation Utils

+(BOOL)isLargeScreen{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return (screenRect.size.height == 568.0) ? YES : NO;
}

+(NSString*)imageNameForTabBarItemSelected:(DesiBeatsTabBarItem)index{

    switch (index) {
            
        case DesiBeatsItemEvents:
            return @"CallusSelected";
            break;
            
        case DesiBeatsItemNews:
            return @"NewsSelected";
            break;
            
        case DesiBeatsItemRadio:
            return @"RadioSelected";
            break;
            
        case DesiBeatsItemRequest:
            return @"RequestSelected";
            break;
            
        case DesiBeatsItemMore:
            return @"MoreSelected";
            break;
            
        default:
            return @"";
            break;
    }
}

+(NSString*)imageNameForTabBarItemUnSelected:(DesiBeatsTabBarItem)index{
    switch (index) {
            
        case DesiBeatsItemEvents:
            return @"CallusUnSelected";
            break;
            
        case DesiBeatsItemNews:
            return @"NewsUnselected";
            break;
            
        case DesiBeatsItemRadio:
            return @"RadioUnSelected";
            break;
            
        case DesiBeatsItemRequest:
            return @"RequestUnSelected";
            break;
            
        case DesiBeatsItemMore:
            return @"MoreUnSelected";
            break;
            
        default:
            return @"";
            break;
    }
}


+(NSString*)titleForTabBarIndex:(DesiBeatsTabBarItem)index{
   
    switch (index) {
            
        case DesiBeatsItemEvents:
            return @"Events";
            break;

        case DesiBeatsItemNews:
            return @"News";
            break;
        
        case DesiBeatsItemRadio:
            return @"Radio";
            break;

        case DesiBeatsItemRequest:
            return @"Request";
            break;

        case DesiBeatsItemMore:
            return @"More";
            break;

        default:
            return @"";
            break;
    }
}

+(UITabBarItem*)tabbarButtonItemForIndex:(DesiBeatsTabBarItem)indexForItem{
    
    UITabBarItem *tabBarItemForIndex = nil;

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIImage *selectedImage  = [UIImage imageNamed:[Utils imageNameForTabBarItemSelected:indexForItem]];
        UIImage *unSeletedImage = [UIImage imageNamed:[Utils imageNameForTabBarItemUnSelected:indexForItem]];
        
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        unSeletedImage = [unSeletedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        [Utils titleForTabBarIndex:indexForItem]
        tabBarItemForIndex = [[UITabBarItem alloc] initWithTitle:nil image:unSeletedImage tag:indexForItem];
        [tabBarItemForIndex setImage:unSeletedImage];
        [tabBarItemForIndex setSelectedImage:selectedImage];
        
//        UIFont *font = [UIFont boldSystemFontOfSize:15];
//
//        [tabBarItemForIndex setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                    font, NSFontAttributeName,
//                                                    [UIColor darkGrayColor], NSForegroundColorAttributeName,
//                                                    nil] forState:UIControlStateNormal];
//        [tabBarItemForIndex setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                    font, NSFontAttributeName,
//                                                    [UIColor redColor], NSForegroundColorAttributeName,
//                                                    nil] forState:UIControlStateSelected];
    
    }
    else
    {
        
       /* tabBarItemForIndex = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:[Utils imageNameForTabBarItemUnSelected:indexForItem]] tag:indexForItem];
        
        [tabBarItemForIndex setFinishedSelectedImage:[UIImage imageNamed:[Utils imageNameForTabBarItemSelected:indexForItem]] withFinishedUnselectedImage:[UIImage imageNamed:[Utils imageNameForTabBarItemUnSelected:indexForItem]]];*/
        
//        UIFont *font = [UIFont boldSystemFontOfSize:15];
//
//        
//        [tabBarItemForIndex setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                    font, UITextAttributeFont,
//                                                    [UIColor whiteColor], UITextAttributeTextColor,
//                                                    nil] forState:UIControlStateNormal];
//        [tabBarItemForIndex setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                    font, UITextAttributeFont,
//                                                    [UIColor redColor], UITextAttributeTextColor,
//                                                    nil] forState:UIControlStateSelected];
    }
    if (tabBarItemForIndex)
        [tabBarItemForIndex setTag:indexForItem];
    
    tabBarItemForIndex.imageInsets = UIEdgeInsetsMake(0, 0, -12, 0);
    return tabBarItemForIndex;
    
}

+(BOOL)isNetworkAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    [networkReachability startNotifier];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        TRC_DBG(@"There IS NO internet connection");
        return NO;
    }
    else
    {
        TRC_DBG(@"There IS internet connection");
    }
    return YES;
}

+(void)showNoInterConnectivityMessage{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"No Internet Connectivity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

+(CGFloat)screenHeight{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height;
}

+(CGFloat)screenWidth{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.width;
}

+(BOOL)emailValidation:(NSString*)emailid{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];

}

+(BOOL)mobileNumberValidation:(NSString*)mobileNumber{
    
    NSString *mobileNumberPattern = @"[0123456789][0-9]{8}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    return [mobileNumberPred evaluateWithObject:mobileNumber];
}


@end
