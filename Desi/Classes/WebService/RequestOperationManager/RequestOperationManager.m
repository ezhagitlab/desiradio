//
//  RequestOperationManager.m
//  eMT
//
//  Created by Mani on 3/25/14.
//  Copyright (c) 2014 NPCompete. All rights reserved.
//

#import "RequestOperationManager.h"
#import "ResponseSerializer.h"
#import "RequestSerializer.h"

NSString const *DESI_SONG_TITLE     = @"http://cdn.voscast.com/stats/display.js?key=8bb0d97fb338dc652ffc415b33472ecb&stats=songtitle";

NSString const *SEND_DEVICE_TOKEN_URL   = @"http://lucroapps.com/desibeat/registerdt.php?device_id=";
NSString const *LUCRO_SERVER_PATH       = @"http://www.lucroapps.com/";

NSString const *NEWS_EVENTS_PATH        = @"desibeat/readjson.php";
NSString const *SEND_DEVICE_TOKEN_PATH  = @"desibeat/registerdt.php";

@implementation RequestOperationManager

+(NSURL*)getBaseURL
{
    return [NSURL URLWithString:(NSString*)LUCRO_SERVER_PATH];
}

-(void)customInitilization
{
    self.requestSerializer  = [RequestSerializer sharedInstance];
    self.responseSerializer = [ResponseSerializer sharedInstance];
}

+(RequestOperationManager*)sharedManager
{
    static dispatch_once_t predicate = 0;
    static RequestOperationManager *sharedInstance = nil;
    dispatch_once(&predicate,  ^{
        sharedInstance = [[self alloc] initWithBaseURL:RequestOperationManager.getBaseURL];
        [sharedInstance customInitilization];
    });
    return sharedInstance;
}

@end
