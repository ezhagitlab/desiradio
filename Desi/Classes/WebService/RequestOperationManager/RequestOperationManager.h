//
//  RequestOperationManager.h
//  eMT
//
//  Created by Mani on 3/25/14.
//  Copyright (c) 2014 NPCompete. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

extern NSString const *LUCRO_SERVER_PATH;
extern NSString const *NEWS_EVENTS_PATH;
extern NSString const *SEND_DEVICE_TOKEN_PATH;
extern NSString const *SEND_DEVICE_TOKEN_URL;

@interface RequestOperationManager : AFHTTPRequestOperationManager

+(RequestOperationManager*)sharedManager;
+(NSURL*)getBaseURL;

@end
