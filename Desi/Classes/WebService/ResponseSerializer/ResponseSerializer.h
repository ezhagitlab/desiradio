//
//  ResponseSerializer.h
//  eMT
//
//  Created by Mani on 3/25/14.
//  Copyright (c) 2014 NPCompete. All rights reserved.
//

#import "AFURLResponseSerialization.h"

@interface ResponseSerializer : AFJSONResponseSerializer

+(ResponseSerializer*)sharedInstance;

@end
