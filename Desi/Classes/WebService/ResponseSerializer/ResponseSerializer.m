//
//  ResponseSerializer.m
//  eMT
//
//  Created by Mani on 3/25/14.
//  Copyright (c) 2014 NPCompete. All rights reserved.
//

#import "ResponseSerializer.h"

@implementation ResponseSerializer

-(void)customInitilization
{
    self.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript", nil];
}

+(ResponseSerializer*)sharedInstance
{
    static dispatch_once_t predicate = 0;
    static ResponseSerializer *sharedInstance = nil;
    dispatch_once(&predicate,  ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance customInitilization];
    });
    return sharedInstance;
}

@end
