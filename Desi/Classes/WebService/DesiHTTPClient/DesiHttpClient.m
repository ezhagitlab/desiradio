//
//  DesiHttpClient.m
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "DesiHttpClient.h"
#import "Constants.h"
#import <AFNetworking/AFJSONRequestOperation.h>

@implementation DesiHttpClient

-(id)init{
    
    if(self = [super initWithBaseURL:[NSURL URLWithString:DesiBaseURL]]){
        [self setParameterEncoding:AFJSONParameterEncoding];
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    }
    return self;
}

 

+(id)sharedInstance{
    static DesiHttpClient *sharedInstance = nil;
    static dispatch_once_t *token = nil;
    dispatch_once(token, ^{
        sharedInstance = [self init];
        
    });
    return sharedInstance;
}


@end
