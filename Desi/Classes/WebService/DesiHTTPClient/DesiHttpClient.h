//
//  DesiHttpClient.h
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "AFHTTPClient.h"

@interface DesiHttpClient : AFHTTPClient
+(id)sharedInstance;

@end
