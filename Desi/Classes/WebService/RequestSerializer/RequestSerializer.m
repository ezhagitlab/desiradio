//
//  RequestSerializer.m
//  eMT
//
//  Created by Mani on 3/25/14.
//  Copyright (c) 2014 NPCompete. All rights reserved.
//

#import "RequestSerializer.h"

@implementation RequestSerializer

-(void)customInitilization
{
    
    [self setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
}

+(RequestSerializer*)sharedInstance
{
    static dispatch_once_t predicate = 0;
    static RequestSerializer *sharedInstance = nil;
    dispatch_once(&predicate,  ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance customInitilization];
    });
    return sharedInstance;
}


@end
