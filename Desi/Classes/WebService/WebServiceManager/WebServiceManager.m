//
//  WebServiceManager.m
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "WebServiceManager.h"
#import "RequestOperationManager.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Logger.h"

@interface WebServiceManager()

@property(nonatomic, strong) SKPSMTPMessage *emailMessage;
@property(nonatomic, strong) UIView *view;
@end

@implementation WebServiceManager

+(id)sharedInstance{
    static WebServiceManager *sharedInstance = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedInstance = [[self alloc ]init];
    });
    return sharedInstance;
}
/*
-(SKPSMTPMessage*)emailMessage
{
    if(!_emailMessage)
        _emailMessage = [[SKPSMTPMessage alloc]init];
    return _emailMessage;
}
*/
-(void)getSongTitleRequestHandler:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:(NSString*)DESI_SONG_TITLE_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock)
        {
            completionBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failureBlock)
        {
            failureBlock(operation, error);
        }
    }];
}

-(void)getNewsAndEventsRequestHandler:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock
{
    [[RequestOperationManager sharedManager] GET:(NSString*)NEWS_EVENTS_PATH parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        TRC_DBG(@"Response object : %@", responseObject);
        completionBlock(operation, responseObject);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        failureBlock(operation, error);
    }];
}

/*
-(void)sendDeviceTokenHandler:(NSString*)deviceToken successBlock:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock
{
    
//    NSString *deviceId = [NSString stringWithFormat:@"%@?device_id=%@",(NSString*)SEND_DEVICE_TOKEN_PATH,deviceToken];
    
    if(!deviceToken)
        return;
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:deviceToken forKey:@"device_id"];
    
    [[RequestOperationManager sharedManager] POST:(NSString*)SEND_DEVICE_TOKEN_PATH parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         TRC_DBG(@"Response object (Sending device token) : %@", responseObject);
         completionBlock(operation, responseObject);
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         failureBlock(operation, error);
     }];
}*/

-(void)sendDeviceTokenHandler:(NSString*)deviceToken successBlock:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", (NSString*)SEND_DEVICE_TOKEN_URL, deviceToken];
    
    TRC_DBG(@"%@", urlString);
    
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock)
        {
            completionBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failureBlock)
        {
            failureBlock(operation, error);
            TRC_DBG(@"");
        }
    }];

}

-(void) sendEmailInBackground:(NSString*)messageBody isSongRequest:(BOOL)isSongReq  clearTextFieldsBlock:(clearFieldsBlock)clearFieldsBlock
{
    
    TRC_DBG(@"Start Sending");
    
    self.clearTextFields = clearFieldsBlock;
    
    
    self.emailMessage                    = [[SKPSMTPMessage alloc]init];
    self.emailMessage.fromEmail          = (NSString*)eMailIdSender; //sender email address
    self.emailMessage.toEmail            = (NSString*)DESI_BEATS_EMAIL;  //receiver email address
    self.emailMessage.relayHost          = @"smtp.gmail.com";
    
    //emailMessage.ccEmail =@"your cc address";
    //emailMessage.bccEmail =@"your bcc address";
    
    self.emailMessage.requiresAuth = YES;
    self.emailMessage.login  = (NSString*)eMailIdSender; //sender email address
    self.emailMessage.pass   = (NSString*)eMailIdSenderPwd; //sender email password
    
    self.emailMessage.subject = (isSongReq) ? (NSString*)eMailSubSongRequest : (NSString*)eMailSubFeedBack;
    self.emailMessage.wantsSecure = YES;
    self.emailMessage.delegate = self; // you must include <SKPSMTPMessageDelegate> to your class

    // Now creating plain text email message
    NSDictionary *plainMsg = [NSDictionary
                              dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
                              messageBody,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
   self.emailMessage.parts = [NSArray arrayWithObjects:plainMsg,nil];

    [self.emailMessage send];
    // sending email- will take little time to send so its better to use indicator with message showing sending...
}

-(void)messageSent:(SKPSMTPMessage *)message
{
    TRC_DBG(@"delegate - message sent");

    if(self.clearTextFields)
        self.clearTextFields();
        
    NSString *messageStr     = ([message.subject isEqualToString:(NSString*)eMailSubFeedBack]) ? (NSString*)eMailResponseFeedBack : (NSString*)eMailResponseSongRequest;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thank You!" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

-(void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    TRC_DBG(@"delegate - message failed");
    
    if(self.clearTextFields)
        self.clearTextFields();
    
    // open an alert with just an OK button
    NSString *messageStr     = ([message.subject isEqualToString:(NSString*)eMailSubFeedBack]) ? (NSString*)kFeedBackFailed : (NSString*)kSongRequestFailed;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    TRC_DBG(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
}

@end
