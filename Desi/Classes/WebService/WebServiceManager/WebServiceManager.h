//
//  WebServiceManager.h
//  Desi
//
//  Created by Jai on 15/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPRequestOperation.h>

#import "SKPSMTPMessage.h"
#import "NSData+Base64Additions.h"

typedef void(^successBlock)(AFHTTPRequestOperation *operation, id responseObject);
typedef void(^failureBlock)(AFHTTPRequestOperation *operation, NSError *err);
typedef void(^clearFieldsBlock)();

@interface WebServiceManager : NSObject<SKPSMTPMessageDelegate>

@property(nonatomic, copy) clearFieldsBlock clearTextFields;

+(id)sharedInstance;

-(void)getSongTitleRequestHandler:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock;
-(void)getNewsAndEventsRequestHandler:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock;

-(void) sendEmailInBackground:(NSString*)messageBody isSongRequest:(BOOL)isSongReq  clearTextFieldsBlock:(clearFieldsBlock)clearFieldsBlock;

-(void)sendDeviceTokenHandler:(NSString*)deviceToken successBlock:(successBlock)completionBlock failureBlock:(failureBlock)failureBlock;
@end
