//
//  DashBoard.m
//  Desi
//
//  Created by Jai on 14/03/14.
//  Copyright (c) 2014 Jai. All rights reserved.
//

#import "RadioVC.h"
#import "Utils.h"
#import <AVFoundation/AVFoundation.h>

@interface RadioVC ()

@property (nonatomic, strong) AVQueuePlayer *desiPlayer;
@property (nonatomic, weak) IBOutlet UIButton *playPauseBtn;
@property(nonatomic) BOOL isPlaying, isPaused;

-(IBAction)playPauseBtnPressed:(id)sender;
@end

@implementation RadioVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.tabBarItem = [Utils tabbarButtonItemForIndex:DesiBeatsItemRadio];

    return self;
}

-(IBAction)playPauseBtnPressed:(id)sender{
    
    if(self.isPlaying){
        
        [self.playPauseBtn setImage:[UIImage imageNamed:@"button_play.png"] forState:UIControlStateNormal];
        
//        [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"button_play.png"] forState:UIControlStateNormal];
        NSLog(@"Radio paused");
        [self.desiPlayer pause];
        self.isPaused  = YES;
        self.isPlaying = NO;
    }else if(self.isPaused){
        
        NSLog(@"Radio played");
        [self.playPauseBtn setImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];
        [self.desiPlayer play];
        self.isPaused  = NO;
        self.isPlaying = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.desiPlayer = [[AVQueuePlayer alloc]initWithURL:[NSURL URLWithString:@"http://s5.voscast.com:8040/;stream.mp3"]];
    [self.desiPlayer play];
    self.isPlaying = YES;
    NSLog(@"Player started");
    [self.navigationItem setTitle:@"Radio"];
    
//    [self.playPauseBtn setImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];

    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];
    
    
    [[self.navigationController navigationBar] setBackgroundImage:[UIImage imageNamed:@"NavBarBackGd.png"] forBarMetrics:UIBarMetricsDefault];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
